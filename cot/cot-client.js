/**
 * C8y cli module
 */
const request = require('request');
const config = require('config');

// const { FetchClient } = require("@c8y/client");
// const { BasicAuth } = require("@c8y/client");
// const { InventoryService } = require("@c8y/client");
// const { IdentityService } = require("@c8y/client");
// const { MeasurementService } = require("@c8y/client");

//const COT_DEVICE_TYPE = 'c8y_Linux';

let _cotClient;
let _inventoryService;
let _identityService;
let _measurementService;

// function initClient() {
//     let auth = new BasicAuth({
//         user: config.COT_USER,
//         password: config.COT_PASSWORD,
//         tenant: config.COT_TENANT
//     });

//     _cotClient = new FetchClient(auth, config.COT_BASEURL);
//     if(_cotClient) {
//         console.log('Cumulocity client successfuly initialized!');
//         _inventoryService = new InventoryService(_cotClient);
//         console.log('Inventory service successfuly initialized!');
//         _identityService = new IdentityService(_cotClient);
//         console.log('Identity service successfuly initialized!');
//         _measurementService = new MeasurementService(_cotClient);
//         console.log('Measurement service successfuly initialized!');
//     }
// };

// function createDeviceAndExternalId(deviceName, deviceType, externalId) {
//     //TODO: initClient is called everytime - for temp use - change to single init call
//     initClient();
//     _inventoryService.create({ name: deviceName, c8y_IsDevice: {}, type: deviceType}).then(function (data) {
//         console.log('_inventoryService create data: '  + data.data.id);
//         _identityService.create({ type: config.COT_DEVICE_TYPE, externalId: externalId, managedObject: {id: data.data.id}}).catch(function (errorMessage) {
//             log.error('Error on identySevice cretae: ' + errorMessage);
//         }).then(function (identityData) {
//             console.log('new device created: ' + JSON.stringify(identityData));
//             return identityData.data;
//         });
//     });
// }


async function createMeasurement(measurements) {
    const url = `${config.COT_BASEURL}measurement/measurements`;
    const fetchOptions = {
      method: 'POST',
      body: JSON.stringify(measurements),
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/vnd.com.nsn.cumulocity.measurementCollection+json',
        'Authorization': 'Basic TVJVVFlVTkpBWVlBLkRFVkFOQUxBTUFUSEB0LXN5c3RlbXMuY29tOkNvbnZfc2VwdDIwMjA='
      }
    }
    if (config.HTTP_PROXY) {
        fetchOptions.proxy = config.HTTP_PROXY;
    }
    return new Promise((resolve,reject)=>{
        request.post(url,fetchOptions,(err,res,body)=>{
            if(err){
                console.log(err);
                reject(err);
            }
            console.log(body);
            console.log(res.statusCode);
            resolve(res);
        })
    })
    
    // return request.post(url, fetchOptions).then(result => {
    //   if (result.status >= 200 && result.status < 300) {
    //     return Promise.resolve(result.json());
    //   } else {
    //     return Promise.reject(result);
    //   }
    // }, error => {
    //   return Promise.reject(error);
    // });

    
}

// function getClient () {
//     return _cotClient;
// }

// function getInventoryService () {
//     return _inventoryService;
// }

// function getIdentityService () {
//     return _identityService;
// }

// function getMeasurementService () {
//     return _measurementService;
// }

//  async function getManagedObjByExternalId(customId) {
//     initClient();
//   console.log('getManagedObjByExternalId starting ... ');
//   const exIdentity = {
//    type: config.COT_DEVICE_TYPE,
//    externalId: customId
//   };
//   console.log(`search managedObj by exIdentity: ${JSON.stringify(exIdentity)}`);
//   return new Promise(async (resolve) => {
//    const { data, res } = await _identityService.detail(exIdentity);
//    if (res.status === 200) {
//     resolve(data);
//    } else if (res.status === 400) {
//     console.log('Error in getManagedObjByExternalId', res);
//     resolve(null);
//    }
//   }).catch((error) => {
//    // TODO: ignore the Error ETimedOut error
//    log.warn(`temp error from request ignored:${error}`);
//   });
//  }


async function getManagedObjByExternalId(customId) {
    return new Promise(async (resolve, reject) => {
      const bootstrapAuth = {
        user: config.COT_USER,
        pass: config.COT_PASSWORD,
        sendImmediately: true
      };
      const requestOptions = {
        url: `${config.COT_BASEURL}identity/externalIds/${config.COT_DEVICE_TYPE}/${customId}`,
        json: true,
        auth: bootstrapAuth
      };
      if (config.HTTP_PROXY) {
        requestOptions.proxy = config.HTTP_PROXY;
      }
      request.get(requestOptions, (err, response, body) => {
        try {
          if (err) {
            console.log(`Error occured at getting identity from CoT: External Id: ExternalId=${customId}`);
            resolve(null);
          }
          if (response && response.statusCode === 200) {
            console.log(body);
            const result = body;
           if (result) {
              resolve(result);
            } else {
              resolve(null);
            }
          } else {
            console.log({ statusCode: response.statusCode },
              `GET ${config.COT_BASEURL}identity/externalIds/${config.COT_DEVICE_TYPE}/${customId} at COT `);
            resolve(null);
          }
        } catch (error) {
          // TODO: ignore the Error ETimedOut error
          console.log(`temp error from request ignored:${error}`);
        }
      });
    }).catch((error) => {
      // TODO: ignore the Error ETimedOut error
      console.log(`temp error from request ignored:${error}`);
    });
  }


module.exports = {
    getManagedObjByExternalId,
    createMeasurement
}
