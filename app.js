const request = require('request');
const config = require('config');
const cron = require('node-cron');
const cot = require('./cot/cot-client');
const {buildRequestHeader, createMeasurementValues}  = require('./Utils/utils');
const {getDevicesFromBackend, getAnalogSignal, getDistanceAnalogValues} = require('./kogerBackend/bakendAPI');

async function getDeviceList(){
    
    let deviceList = await getDevicesFromBackend();
    console.log('Device List: ', deviceList);
    for(let device of deviceList){
        // For All three Devices
        let deviceElementId = device.id;
        let customId = device.name;

        if (customId === 'Weather-API'){
            break;
        }
        // For Temperatures
        //let deviceElementId = deviceList[0].id;
        //let customId = deviceList[0].name;


        // for Distance Device
        //let deviceElementId = deviceList[1].id;
        //let customId = deviceList[1].name;

        // For UV-INDEX
        //let deviceElementId = deviceList[2].id;
        //let customId = deviceList[2].name;
        console.log('Devices: ', device)
        let finalCotResult = await sendDataToCoT( deviceElementId, customId);
        console.log("Final Result", finalCotResult);
            if(finalCotResult === null){
                continue;
            }else{
                continue;
            }
            
        }

}

async function sendDataToCoT(deviceElementId, customId){
    return new Promise(async (resolve, reject)=>{
        let analogSignal = await getAnalogSignal( deviceElementId);
        console.log('Analog signal: ', analogSignal);
        for(let signal of analogSignal.analogSignals){

            let distanceAnalogSignal = signal.id;
            //let distanceAnalogSignal = analogSignal.analogSignals[1].id;
            let ditanceValues = await getDistanceAnalogValues(deviceElementId, distanceAnalogSignal);
            if(ditanceValues.length > 0){
                console.log('Custom ID: ', customId);
                let deviceIdData = await cot.getManagedObjByExternalId(customId);
                let deviceId = deviceIdData.managedObject.id;
                console.log('Device ID: ', deviceId);
                for(let deviceValues of ditanceValues){
                    
                    console.log('deviceValues:',deviceValues);
                    let measurementValues  = createMeasurementValues(deviceValues, distanceAnalogSignal, deviceId);
                    console.log(JSON.stringify(measurementValues));
                    const measurement = await cot.createMeasurement(measurementValues);
                    if(measurement.statusCode >= 200 && measurement.statusCode < 300){
                        console.log('Send to COT success: ');
                    }else{
                        console.log('Failed to post value');
                       
                    }
                }
                } else{
                    console.log('No Data available in 15 mins interval');
                    return reject(null);
                }
            }
            resolve('success');
    })
}

cron.schedule('*/15 * * * *', () => {
    getDeviceList();
  });

  //getDeviceList();

//   setInterval(function(){ 
// 	console.log("Hi, after 15 Minutes"); 
// },900000) 
