# FROM mhart/alpine-node as intermediate

# # install python and node-gyp
# RUN apk --no-cache add --virtual native-deps \
#   g++ gcc libgcc libstdc++ linux-headers make python 

# RUN npm install --quiet node-gyp -g

# # Install app dependencies
# WORKDIR /usr/src/app
# COPY connector-app/package.json ./
# RUN npm install

# # -----------------------------------------------------------------
# FROM mhart/alpine-node

# WORKDIR /usr/src/app

# # Bundle app source
# COPY connector-app/ ./
# # Copy npm packages from build container
# COPY --from=intermediate /usr/src/app/node_modules .

# CMD npm start

FROM node:alpine

WORKDIR /usr/app

COPY ./package.json ./
RUN npm install
COPY ./ ./

CMD ["npm", "start"]
