const request = require('request');
const config = require('config');
const {buildRequestHeader, createMeasurementValues}  = require('../Utils/utils');

const URL = config.KOGERPOLDERBRUG_API


function getDevicesFromBackend(){
    let requestURL = buildRequestHeader(URL);
    if (config.HTTP_PROXY) {
        requestURL.proxy = config.HTTP_PROXY;
    }
    return new Promise((resolve,reject)=>{
        request.get(requestURL, (err, res,body)=>{
            if(err){
                console.log(err);
                reject(err)
            }
            //console.log(res);
            if(res.statusCode === 200){
                let deviceList = JSON.parse(body);
                //console.log(deviceList);
                resolve(deviceList);
            }
        })
    })
}

function getAnalogSignal( deviceElementId){
    let requestURL = buildRequestHeader(URL);
    if (config.HTTP_PROXY) {
        requestURL.proxy = config.HTTP_PROXY;
    }
    return new Promise((resolve,reject)=>{
        requestURL.url = `${requestURL.url}/${deviceElementId}`;
        //requestURL.url = 'https://kogerpolderbrug.joostentessa.nl/api/partner/elements/2';
        //console.log('Request URL:', requestURL);
        request.get(requestURL, (err, res,body)=>{
            if(err){
                console.log(err);
                reject(err)
            }
            //console.log(res);
            if(res.statusCode === 200){
                let analogElement = JSON.parse(body);
                //console.log(deviceList);
                resolve(analogElement);
            }
        })
    })
}

function getDistanceAnalogValues( deviceElementId, distanceAnalogSignal){
    let requestURL = buildRequestHeader(URL);
    if (config.HTTP_PROXY) {
        requestURL.proxy = config.HTTP_PROXY;
    }
    let endTime = new Date().toISOString().split('Z')[0];
    let startTime = new Date();

    // For 1 day refresh rate
    //startTime.setDate(startTime.getDate() - 3);
    //startTime = startTime.toISOString().split('Z')[0]

    // For 15 min refresh date
    startTime.setMinutes(startTime.getMinutes() - 15);
    startTime = startTime.toISOString().split('Z')[0]

    return new Promise((resolve,reject)=>{
        requestURL.url = `${requestURL.url}/${deviceElementId}/analog/${distanceAnalogSignal}/values?startTime=${startTime}&endTime=${endTime}`;
        console.log('Request URL:', requestURL);
        request.get(requestURL, (err, res,body)=>{
            if(err){
                console.log(err);
                reject(err);
            }
            //console.log(res);
            if(res.statusCode === 200){
                let ditanceValues = JSON.parse(body);
                //console.log(deviceList);
                resolve(ditanceValues);
            }
        })
    })
}

module.exports = {
    getDevicesFromBackend,
    getAnalogSignal,
    getDistanceAnalogValues
}