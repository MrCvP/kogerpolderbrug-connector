const config = require('config');

function buildRequestHeader(url) {
    return {
      url,
      headers: {
        Authorization: config.AUTH
      }
    }
}

function createMeasurementValues(analogValues, analogId, deviceId){
    let analogMeasurement = null;
    switch(analogId){
        case 1:
            analogMeasurement = {
                "measurements": [
                    {
                        "time": new Date(analogValues.time).toISOString(),
                        "source": {
                            "id": deviceId
                        },
                        "type": "c8y_Serial",
                        "attrs": {
                            "c8y_Temperatur-water": {
                                "V": {
                                    "value": analogValues.Value,
                                    "unit": "Celsius"
                                }
                            }
                        }
                    }
                ]
            }
            break;
        case 2:
            analogMeasurement = {
                "measurements": [
                    {
                        "time": new Date(analogValues.time).toISOString(),
                        "source": {
                            "id": deviceId
                        },
                        "type": "c8y_Serial",
                        "attrs": {
                            "c8y_Temperatur-outside": {
                                "V": {
                                    "value": analogValues.Value,
                                    "unit": "Celsius"
                                }
                            }
                        }
                    }
                ]
            }
            break;            
        case 3:
            analogMeasurement = {
                "measurements": [
                    {
                        "time": new Date(analogValues.time).toISOString(),
                        "source": {
                            "id": deviceId
                        },
                        "type": "c8y_Serial",
                        "attrs": {
                            "c8y_Distance": {
                                "V": {
                                    "value": analogValues.Value,
                                    "unit": "mm"
                                }
                            }
                        }
                    }
                ]
            };
              break;
        case 4:
            analogMeasurement = {
                "measurements": [
                    {
                        "time": new Date(analogValues.time).toISOString(),
                        "source": {
                            "id": deviceId
                        },
                        "type": "c8y_Serial",
                        "attrs": {
                            "c8y_UV-Index": {
                                "V": {
                                    "value": analogValues.Value,
                                    "unit": "mW"
                                }
                            }
                        }
                    }
                ]
            };
              break;
              default:
                  break;
    }
    return analogMeasurement;
}

module.exports = {
    buildRequestHeader,
    createMeasurementValues
}